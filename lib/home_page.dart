import 'package:flutter/material.dart';
import 'package:flutter_gym_app/database_helper.dart';

import 'my_dialog.dart';
import 'package:flutter_gym_app/SessionData.dart';
import 'package:flutter_gym_app/session_row.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  State<StatefulWidget> createState() {
    return _MyHomePageState();
  }
}

enum actions { exercises, help }

class _MyHomePageState extends State<MyHomePage> {
  List<GymSessionData> sessions = [];

  Future<GymSessionData> _openDialog([GymSessionData session]) async {
    return await showDialog(
        context: context,
        builder: (BuildContext context) {
          return myCustomDialog(context, session);
        });
  }

  void addSessionToList(session) {
    setState(() {
      sessions.add(session);
    });
  }

  void editSessionInList(index, newSession) {
    DatabaseHelper.db.update(newSession).then(
      (result) {
        if (result == 1)
          setState(() {
            sessions[index] = newSession;
          });
      },
    );
  }

  void deleteSessionFromList(index) {
    DatabaseHelper.db.delete(sessions[index].id).then((deletedRows) {
      if (deletedRows > 0) {
        setState(() {
          sessions.removeAt(index);
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    // This gets all existing data in db when the app first starts
    DatabaseHelper.db.read().then((sessionsList) {
      setState(() {
        sessions.addAll(sessionsList);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xEFFFFFFF),
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text(widget.title),
        actions: <Widget>[
          PopupMenuButton(
            onSelected: (value) {
              if (value == actions.exercises) {
                print("exercises");
              } else if (value == actions.help) {
                print("help");
              } else {
                print("YOU SHOULDN'T BE HERE AHH");
              }
            },
            icon: Icon(Icons.more_vert),
            itemBuilder: (BuildContext context) => [
              PopupMenuItem(
                child: Text("Exercises"),
                value: actions.exercises,
              ),
              PopupMenuItem(
                child: Text("Help"),
                value: actions.help,
              ),
            ],
          ),
        ],
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
//              reverse: true,
              itemCount: sessions.length,
              itemBuilder: (context, index) {
                return sessionRow(
                  sessionData: sessions[index],
                  onDeleteSelected: () {
                    deleteSessionFromList(index);
                  },
                  onEditSelected: () {
                    _openDialog(sessions[index]).then(
                      (newSession) => {
                        if (newSession != null)
                          {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: Text("Data edited!"),
                            )),
                            // edit data
                            editSessionInList(index, newSession),
                          }
                      },
                    );
                  },
                );
                GestureDetector(
//                  child: sessionRow(sessions[index]),
//                  Dismissible(
//                    direction: DismissDirection.endToStart,
//                    background: Card(
//                      color: Colors.red,
//                    ),
//                    onDismissed: (direction) {
//                      print(direction);
//                      if (direction == DismissDirection.endToStart) {
//                        print("true baby");
//                        deleteSessionFromList(index);
//                      }
//                    },
//                    key: Key(sessions[index].id.toString()),
//                    child: sessionRow(sessions[index]),
//                  ),
//                  onLongPress: () {
//                    _openDialog(sessions[index]).then(
//                      (newSession) => {
//                        if (newSession != null)
//                          {
//                            Scaffold.of(context).showSnackBar(SnackBar(
//                              content: Text("Data edited!"),
//                            )),
//                            // edit data
//                            editSessionInList(index, newSession),
//                          }
//                      },
//                    );
//                  },
//                );
                    );
              },
            ),
          ),
        ],
      ),
      floatingActionButton: Builder(builder: (context) {
        return FloatingActionButton(
          backgroundColor: Colors.green,
          onPressed: () {
            _openDialog().then((session) => {
                  if (session != null)
                    {
                      Scaffold.of(context).showSnackBar(SnackBar(
                        content: Text("New data has been added!"),
                      )),
                      DatabaseHelper.db.create(session).then((storedSession) {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: Text("Stored data has id " +
                              storedSession.id.toString()),
                        ));
                        addSessionToList(storedSession);
                      })
                    }
                });
          },
          tooltip: 'Add session',
          child: Icon(Icons.add),
        );
      }),
    );
  }
}
