// TODO: LEARN TO MAKE A VALIDATOR AND ONSAVE FUNCTION
//import 'package:flutter/material.dart';
//
//class CustomDropDownButton extends StatefulWidget {
//  CustomDropDownButton({Key key, this.onExerciseChange, this.initialValue})
//      : super(key: key);
//
//  final String initialValue;
//  final ValueChanged<String> onExerciseChange;
//
//  @override
//  State<StatefulWidget> createState() {
//    return _CustomDropDownButtonState();
//  }
//}
//
//class _CustomDropDownButtonState extends State<CustomDropDownButton> {
//  String val;
//  String getValue() {
//    if (val == null) {
//      val = widget.initialValue;
//    }
//    return val;
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return DropdownButtonFormField(
//      hint: Text("Exercise machine"),
//      value: getValue(),
//      //widget.onExerciseChange,
//      items: [
//        DropdownMenuItem(
//          child: Text("Treadmill"),
//          value: "Treadmill",
//        ),
//        DropdownMenuItem(
//          child: Text("Bike"),
//          value: "Bike",
//        ),
//        DropdownMenuItem(
//          child: Text("XT"),
//          value: "XT",
//        ),
//      ],
//      onChanged: (selectedValue) {
//        setState(() {
//          val = selectedValue;
//          widget.onExerciseChange(selectedValue);
//        });
//        //newSession.exercise = selectedValue;
//      },
//      validator: (value) {
//        if (value == null) {
//          return "Please enter a valid machine!";
//        }
//        return null;
//      },
//    );
//  }
//}
