import 'package:flutter/material.dart';

import 'SessionData.dart';
import 'package:intl/intl.dart';

Widget myCustomDialog(BuildContext context, GymSessionData oldSession) {
  final _formKey = GlobalKey<FormState>();
  GymSessionData session = GymSessionData();
  if (oldSession != null) session = oldSession;

  String getWeight() {
    if (session.weight == null)
      return null;
    else
      return session.weight.toString();
  }

  String getDate() {
    if (session.date == null)
      return DateFormat('d MMM, yyyy').format(DateTime.now());
    else
      return session.date;
  }

  String getExercise() {
    if (session.exercise == null)
      return null;
    else
      return session.exercise;
  }

  String getDuration() {
    if (session.duration == null)
      return null;
    else
      return session.duration.toString();
  }

  Widget _buildWeight() {
    return TextFormField(
      initialValue: getWeight(),
      keyboardType: TextInputType.number,
      decoration: const InputDecoration(
        hintText: 'Current weight (Kg)',
      ),
      validator: (value) {
        double result = double.tryParse(value);
        if (result == null) {
          //value.isEmpty
          return 'Please enter a valid weight';
        }
        return null;
      },
      onSaved: (String value) {
        session.weight = double.parse(value);
      },
    );
  }

  // TODO: Date picker dialog? ... InputDatePickerFormField ??
  Widget _buildDate() {
    return TextFormField(
      initialValue: getDate(), //newSession.date,
      decoration: const InputDecoration(
        hintText: 'Session Date',
      ),
      validator: (value) {
        if (value.isEmpty) {
          return 'Please enter the date of the session';
        }
        return null;
      },
      onSaved: (String value) {
        session.date = value;
      },
    );
  }

  Widget _buildExercise() {
    return DropdownButtonFormField(
      hint: Text("Exercise machine"),
      value: getExercise(),
      items: [
        DropdownMenuItem(
          child: Text("Treadmill"),
          value: "Treadmill",
        ),
        DropdownMenuItem(
          child: Text("Bike"),
          value: "Bike",
        ),
        DropdownMenuItem(
          child: Text("XT"),
          value: "XT",
        ),
      ],
      // this just needs to run, it doesn't matter whats in it
      onChanged: (selectedValue) {},
      validator: (value) {
        if (value == null) {
          return "Please enter a valid machine!";
        }
        return null;
      },
      onSaved: (String value) {
        session.exercise = value;
      },
    );
  }

  Widget _buildDuration() {
    return TextFormField(
      initialValue: getDuration(),
      //oldSession.duration.toString(),
      keyboardType: TextInputType.number,
      decoration: const InputDecoration(
        hintText: 'Exercise Duration (min)',
      ),
      validator: (value) {
        int result = int.tryParse(value);
        if (result == null) {
          return 'Please enter a valid session duration';
        }
        return null;
      },
      onSaved: (String value) {
        session.duration = int.parse(value);
      },
    );
  }

  return SimpleDialog(
    title: const Text('Add Session data'),
    children: <Widget>[
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _buildWeight(),
              _buildDate(),
              _buildExercise(),
              _buildDuration(),
              Padding(
                padding: EdgeInsets.only(top: 10),
                child: Row(
                  children: <Widget>[
                    RaisedButton(
                      //   ~~~~~~~ Submit ~~~~~~~
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          _formKey.currentState.save();

                          // Close dialog
                          Navigator.pop(context, session);
                        }
                      },
                      child: Text('Submit'),
                    ),
                    RaisedButton(
                      //   ~~~~~~~ Cancel ~~~~~~~
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text('Cancel'),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    ],
  );
}

//
//class MyCustomDialog extends StatelessWidget {
//  final GymSessionData newSession = GymSessionData();
//  final GymSessionData oldSession;
//  final _formKey = GlobalKey<FormState>();
//
//  MyCustomDialog([this.oldSession]) {
//    if (oldSession != null) newSession.id = oldSession.id;
//  }
//
//  String getInitialWeight() {
//    if (oldSession == null)
//      // return null so the hint is showed
//      return null;
//    else
//      return oldSession.weight.toString();
//  }
//
//  String getInitialDate() {
//    if (oldSession == null)
//      return DateFormat('d MMM, yyyy').format(DateTime.now());
//    else
//      return oldSession.date.toString();
//  }
//
//  String getInitialExercise() {
//    if (oldSession == null)
//      return null;
//    else {
//      // TODO: SOME METHOD TO ENSURE THAT THE INITIAL VALUE IS A "DropdownMenuItem"
//      return oldSession.exercise.toString();
//    }
//  }
//
//  String getInitialDuration() {
//    if (oldSession == null)
//      return null;
//    else
//      return oldSession.duration.toString();
//  }
//
//  Widget _buildWeight() {
//    return TextFormField(
//      initialValue: getInitialWeight(),
//      keyboardType: TextInputType.number,
//      decoration: const InputDecoration(
//        hintText: 'Current weight (Kg)',
//      ),
//      validator: (value) {
//        double result = double.tryParse(value);
//        if (result == null) {
//          //value.isEmpty
//          return 'Please enter a valid weight';
//        }
//        return null;
//      },
//      onSaved: (String value) {
//        newSession.weight = double.parse(value);
//      },
//    );
//  }
//
//  // TODO: Date picker dialog? ... InputDatePickerFormField ??
//  Widget _buildDate() {
//    return TextFormField(
//      initialValue: getInitialDate(), //newSession.date,
//      decoration: const InputDecoration(
//        hintText: 'Session Date',
//      ),
//      validator: (value) {
//        if (value.isEmpty) {
//          return 'Please enter the date of the session';
//        }
//        return null;
//      },
//      onSaved: (String value) {
//        newSession.date = value;
//      },
//    );
//  }
//
//  Widget _buildExercise() {
//    return DropdownButtonFormField(
//      hint: Text("Exercise machine"),
//      value: getInitialExercise(),
//      items: [
//        DropdownMenuItem(
//          child: Text("Treadmill"),
//          value: "Treadmill",
//        ),
//        DropdownMenuItem(
//          child: Text("Bike"),
//          value: "Bike",
//        ),
//        DropdownMenuItem(
//          child: Text("XT"),
//          value: "XT",
//        ),
//      ],
//      // this just needs to run, it doesn't matter whats in it
//      onChanged: (selectedValue) {},
//      validator: (value) {
//        if (value == null) {
//          return "Please enter a valid machine!";
//        }
//        return null;
//      },
//      onSaved: (String value) {
//        newSession.exercise = value;
//      },
//    );
//  }
//
//  Widget _buildDuration() {
//    return TextFormField(
//      initialValue: getInitialDuration(),
//      //oldSession.duration.toString(),
//      keyboardType: TextInputType.number,
//      decoration: const InputDecoration(
//        hintText: 'Exercise Duration (min)',
//      ),
//      validator: (value) {
//        int result = int.tryParse(value);
//        if (result == null) {
//          return 'Please enter a valid session duration';
//        }
//        return null;
//      },
//      onSaved: (String value) {
//        newSession.duration = int.parse(value);
//      },
//    );
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return SimpleDialog(
//      title: const Text('Add Session data'),
//      children: <Widget>[
//        Padding(
//          padding: EdgeInsets.symmetric(horizontal: 15),
//          child: Form(
//            key: _formKey,
//            child: Column(
//              crossAxisAlignment: CrossAxisAlignment.start,
//              children: <Widget>[
//                _buildWeight(),
//                _buildDate(),
//                _buildExercise(),
//                _buildDuration(),
//                Padding(
//                  padding: EdgeInsets.only(top: 10),
//                  child: Row(
//                    children: <Widget>[
//                      RaisedButton(
//                        //   ~~~~~~~ Submit ~~~~~~~
//                        onPressed: () {
//                          if (_formKey.currentState.validate()) {
//                            _formKey.currentState.save();
//
//                            // Close dialog
//                            Navigator.pop(context, newSession);
//                          }
//                        },
//                        child: Text('Submit'),
//                      ),
//                      RaisedButton(
//                        //   ~~~~~~~ Cancel ~~~~~~~
//                        onPressed: () {
//                          Navigator.pop(context);
//                        },
//                        child: Text('Cancel'),
//                      ),
//                    ],
//                  ),
//                ),
//              ],
//            ),
//          ),
//        ),
//      ],
//    );
//  }
//}
