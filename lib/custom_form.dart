//import 'package:flutter/material.dart';
//import 'package:intl/intl.dart';
//
//import 'SessionData.dart';
//
//class MyCustomForm extends StatelessWidget {
//  final GymSessionData newSession = GymSessionData();
//  final GymSessionData oldSession;
//
//  MyCustomForm([this.oldSession]) {
//    if (oldSession != null) newSession.id = oldSession.id;
//  }
//
//  String getInitialWeight() {
//    if (oldSession == null)
//      // return null so the hint is showed
//      return null;
//    else
//      return oldSession.weight.toString();
//  }
//
//  String getInitialDate() {
//    if (oldSession == null)
//      return DateFormat('d MMM, yyyy').format(DateTime.now());
//    else
//      return oldSession.date.toString();
//  }
//
//  String getInitialExercise() {
//    if (oldSession == null)
//      return null;
//    else {
//      // TODO: SOME METHOD TO ENSURE THAT THE INITIAL VALUE IS A "DropdownMenuItem"
//      return oldSession.exercise.toString();
//    }
//  }
//
//  String getInitialDuration() {
//    if (oldSession == null)
//      return null;
//    else
//      return oldSession.duration.toString();
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    final _formKey = GlobalKey<FormState>();
//
//    Widget _buildWeight() {
//      return TextFormField(
//        initialValue: getInitialWeight(),
//        keyboardType: TextInputType.number,
//        decoration: const InputDecoration(
//          hintText: 'Current weight (Kg)',
//        ),
//        validator: (value) {
//          double result = double.tryParse(value);
//          if (result == null) {
//            //value.isEmpty
//            return 'Please enter a valid weight';
//          }
//          return null;
//        },
//        onSaved: (String value) {
//          newSession.weight = double.parse(value);
//        },
//      );
//    }
//
//    // TODO: Date picker dialog? ... InputDatePickerFormField ??
//    Widget _buildDate() {
//      return TextFormField(
//        initialValue: getInitialDate(), //newSession.date,
//        decoration: const InputDecoration(
//          hintText: 'Session Date',
//        ),
//        validator: (value) {
//          if (value.isEmpty) {
//            return 'Please enter the date of the session';
//          }
//          return null;
//        },
//        onSaved: (String value) {
//          newSession.date = value;
//        },
//      );
//    }
//
//    Widget _buildExercise() {
//      return DropdownButtonFormField(
//        hint: Text("Exercise machine"),
//        value: getInitialExercise(),
//        items: [
//          DropdownMenuItem(
//            child: Text("Treadmill"),
//            value: "Treadmill",
//          ),
//          DropdownMenuItem(
//            child: Text("Bike"),
//            value: "Bike",
//          ),
//          DropdownMenuItem(
//            child: Text("XT"),
//            value: "XT",
//          ),
//        ],
//        // this just needs to run, it doesn't matter whats in it
//        onChanged: (selectedValue) {},
//        validator: (value) {
//          if (value == null) {
//            return "Please enter a valid machine!";
//          }
//          return null;
//        },
//        onSaved: (String value) {
//          newSession.exercise = value;
//        },
//      );
//    }
//
//    Widget _buildDuration() {
//      return TextFormField(
//        initialValue: getInitialDuration(),
//        //oldSession.duration.toString(),
//        keyboardType: TextInputType.number,
//        decoration: const InputDecoration(
//          hintText: 'Exercise Duration (min)',
//        ),
//        validator: (value) {
//          int result = int.tryParse(value);
//          if (result == null) {
//            return 'Please enter a valid session duration';
//          }
//          return null;
//        },
//        onSaved: (String value) {
//          newSession.duration = int.parse(value);
//        },
//      );
//    }
//
//    return Padding(
//      padding: EdgeInsets.symmetric(horizontal: 15),
//      child: Form(
//        key: _formKey,
//        child: Column(
//          crossAxisAlignment: CrossAxisAlignment.start,
//          children: <Widget>[
//            _buildWeight(),
//            _buildDate(),
//            _buildExercise(),
//            _buildDuration(),
//            Padding(
//              padding: EdgeInsets.only(top: 10),
//              child: Row(
//                children: <Widget>[
//                  RaisedButton(
//                    //   ~~~~~~~ Submit ~~~~~~~
//                    onPressed: () {
//                      if (_formKey.currentState.validate()) {
//                        _formKey.currentState.save();
//
//                        // Close dialog
//                        Navigator.pop(context, newSession);
//                      }
//                    },
//                    child: Text('Submit'),
//                  ),
//                  RaisedButton(
//                    //   ~~~~~~~ Cancel ~~~~~~~
//                    onPressed: () {
//                      Navigator.pop(context);
//                    },
//                    child: Text('Cancel'),
//                  ),
//                ],
//              ),
//            ),
//          ],
//        ),
//      ),
//    );
//  }
//}
