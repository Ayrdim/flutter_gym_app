import 'package:flutter_gym_app/database_helper.dart';

class GymSessionData {
  int id;
  String type = "Cardio";
  double weight;
  String date;
  String exercise;
  int duration;

  GymSessionData(
      {this.id, this.weight, this.date, this.exercise, this.duration});

  // FROM db
  // convenience constructor to create a Word object
  GymSessionData.fromMap(Map<String, dynamic> map) {
    id = map[DatabaseHelper.db.COLUMN_ID];
//    type = map[DatabaseHelper.db.COLUMN_TYPE];
    weight = map[DatabaseHelper.db.COLUMN_WEIGHT];
    date = map[DatabaseHelper.db.COLUMN_DATE];
    exercise = map[DatabaseHelper.db.COLUMN_EXERCISE];
    duration = map[DatabaseHelper.db.COLUMN_DURATION];
  }

  // TO db
  // convenience method to create a Map from this Word object
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      // id is assigned by the db
      DatabaseHelper.db.COLUMN_ID: id,
//      DatabaseHelper.db.COLUMN_TYPE: TYPE,
      DatabaseHelper.db.COLUMN_WEIGHT: weight,
      DatabaseHelper.db.COLUMN_DATE: date,
      DatabaseHelper.db.COLUMN_EXERCISE: exercise,
      DatabaseHelper.db.COLUMN_DURATION: duration,
    };
    if (id != null) {
      map[DatabaseHelper.db.COLUMN_ID] = id;
    }
    return map;
  }

  //debug
  void printData() {
    print("id: " +
        this.id.toString() +
        ", " +
        "weight: " +
        this.weight.toString() +
        ", " +
        "date: " +
        this.date +
        ", " +
        "exercise: " +
        this.exercise +
        ", " +
        "duration: " +
        this.duration.toString());
  }
}
