////import 'package:flutter/cupertino.dart';
//import 'package:flutter/material.dart';
//import 'package:flutter/painting.dart';
//
//import 'SessionData.dart';
//
//class SessionRow extends StatelessWidget {
//  SessionRow(
//      {Key key, this.sessionData, this.onDeleteSelected, this.onEditSelected})
//      : super(key: key);
//  final GymSessionData sessionData;
//  final VoidCallback onDeleteSelected;
//  final VoidCallback onEditSelected;
//
//  @override
//  Widget build(BuildContext context) {
//    return Card(
//      elevation: 5,
//      child: ExpansionTile(
//        title: Text(
//          sessionData.type + " Session", // sessionData.type
//          style: TextStyle(
//            fontWeight: FontWeight.bold,
//          ),
//        ),
//        children: <Widget>[
//          Padding(
//            padding: EdgeInsets.symmetric(horizontal: 20),
//            child: Column(
//              crossAxisAlignment: CrossAxisAlignment.start,
//              children: <Widget>[
//                Divider(),
//                Row(
//                  children: <Widget>[
//                    Text(
//                      "Exercise: " + sessionData.exercise,
//                      style: TextStyle(fontSize: 15),
//                    ),
//                  ],
//                ),
//                Row(
//                  children: <Widget>[
//                    Text(
//                      "Duration: " + sessionData.duration.toString(),
//                      style: TextStyle(fontSize: 15),
//                    ),
//                  ],
//                ),
//                //rowData("Exercise", sessionData.exercise),
//                //rowData("Duration", sessionData.duration.toString()),
//                Divider(),
//              ],
//            ),
//          ),
//          Padding(
//            padding: const EdgeInsets.only(left: 10),
//            child: Row(
//              children: <Widget>[
//                ButtonTheme(
//                  minWidth: 32,
//                  child: FlatButton(
//                    splashColor: Colors.red,
//                    textColor: Colors.red,
//                    onPressed: () {
//                      onDeleteSelected();
//                    },
//                    child: Text("Delete"),
//                  ),
//                ),
//                ButtonTheme(
//                  minWidth: 32,
//                  child: FlatButton(
//                    splashColor: Colors.blue,
//                    textColor: Colors.blue,
//                    onPressed: () {
//                      onEditSelected();
//                    },
//                    child: Text("Edit"),
//                  ),
//                ),
//              ],
//            ),
//          )
//        ],
////      leading: FlutterLogo(),
//        subtitle: Padding(
//          padding: EdgeInsets.only(top: 2),
//          child: Column(
//            crossAxisAlignment: CrossAxisAlignment.start,
//            children: <Widget>[
//              Text(sessionData.date),
//              Text(sessionData.weight.toString() + " Kg")
//            ],
//          ),
//        ),
//      ),
//    );
//  }
//}
