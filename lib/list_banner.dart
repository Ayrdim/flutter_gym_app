import 'package:flutter/material.dart';

Widget listBanner() {

  return Container(
    padding: EdgeInsets.all(20),
    child: Row(
      children: <Widget>[
        Expanded(
          child: Text(
            "Weight (kg)",
            textAlign: TextAlign.center,
          ),
          flex: 1,
        ),
        Expanded(
          child: Text(
            "Date",
            textAlign: TextAlign.center,
          ),
          flex: 1,
        ),
        Expanded(
          child: Text(
            "Duration (min)",
            textAlign: TextAlign.center,
          ),
          flex: 1,
        ),
        Expanded(
          child: Text(
            "Exercise",
            textAlign: TextAlign.center,
          ),
          flex: 1,
        ),
      ],
    ),
  );


}


//
//
//class ListBanner extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      padding: EdgeInsets.all(20),
//      child: Row(
//        children: <Widget>[
//          Expanded(
//            child: Text(
//              "Weight (kg)",
//              textAlign: TextAlign.center,
//            ),
//            flex: 1,
//          ),
//          Expanded(
//            child: Text(
//              "Date",
//              textAlign: TextAlign.center,
//            ),
//            flex: 1,
//          ),
//          Expanded(
//            child: Text(
//              "Duration (min)",
//              textAlign: TextAlign.center,
//            ),
//            flex: 1,
//          ),
//          Expanded(
//            child: Text(
//              "Exercise",
//              textAlign: TextAlign.center,
//            ),
//            flex: 1,
//          ),
//        ],
//      ),
//    );
//  }
//}
