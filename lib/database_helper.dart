import 'dart:async';

import 'SessionData.dart';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  // ignore: non_constant_identifier_names
  final String TABLE_NAME = 'gymSessions';
  // ignore: non_constant_identifier_names
  final String COLUMN_ID = 'id';
  // ignore: non_constant_identifier_names
  final String COLUMN_WEIGHT = 'weight';
  // ignore: non_constant_identifier_names
  final String COLUMN_DATE = 'date';
  // ignore: non_constant_identifier_names
  final String COLUMN_EXERCISE = 'exercise';
  // ignore: non_constant_identifier_names
  final String COLUMN_DURATION = 'duration';

  static final _databaseName = "GymDatabase.db";
  static final _databaseVersion = 1;

//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Not Default stuff ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Make this a singleton class.
  DatabaseHelper._privateConstructor(); // or _()
  static final DatabaseHelper db = DatabaseHelper._privateConstructor();

  // Only allow a single open connection to the database.
  static Database _database;

  // This is called whenever database object is called, this is a getter funct
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await createDatabase();
    return _database;
  }

//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Not Default stuff ~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  // initialise the database
  Future<Database> createDatabase() async {
    return await openDatabase(
      // Set the path to the database.
      join(await getDatabasesPath(), _databaseName),
      // When the database is first created, create a table to store gym data.
      onCreate: (db, version) {
        // Run the CREATE TABLE statement on the database.
        // maybe add await instead of return ?
        return db.execute('''
              CREATE TABLE $TABLE_NAME (
                $COLUMN_ID INTEGER PRIMARY KEY AUTOINCREMENT,
                $COLUMN_WEIGHT DOUBLE NOT NULL,
                $COLUMN_DATE TEXT NOT NULL,
                $COLUMN_EXERCISE TEXT NOT NULL,
                $COLUMN_DURATION INTEGER NOT NULL
              )
              ''');
      },
      // Set the version. This executes the onCreate function and provides a
      // path to perform database upgrades and downgrades.
      version: _databaseVersion,
    );
  }

  Future<GymSessionData> create(GymSessionData session) async {
    final Database db = await database;

    session.id = await db.insert(TABLE_NAME, session.toMap());
    return session; // maybe make this return a bool
  }

  Future<List<GymSessionData>> read() async {
    final Database db = await database;

    // TODO: Eventually think about procedural database fetching?
    final List<Map<String, dynamic>> dbResults = await db.query(TABLE_NAME);

    List<GymSessionData> sessionList = List<GymSessionData>();

    // Maps the resulting db rows to GymSessionData
    dbResults.forEach((dbRow) {
      GymSessionData gymSession = GymSessionData.fromMap(dbRow);
      sessionList.add(gymSession);
    });
    return sessionList;
  }

  Future<int> update(GymSessionData session) async {
    final Database db = await database;

    return await db.update(
      TABLE_NAME,
      session.toMap(),
      // Ensure that the session has a matching id.
      where: "id = ?",
      // Pass the sessions id as a whereArg to prevent SQL injection.
      whereArgs: [session.id],
    );
  }

  Future<int> delete(int id) async {
    final Database db = await database;

    return await db.delete(
      TABLE_NAME,
      where: "id = ?",
      whereArgs: [id],
    );
  }
}
